<?php
get_header();
?>

<div id="contentContainer">
<div id="pageHeader"></div>
<!--content-->
<div id="content">
  <h1><?php echo(get_option('_search_results_text').' "'.$_GET['s'].'"');?></h1>
  <?php

if(have_posts()){
	while(have_posts()){
		the_post();
		echo('  <div class="postBox" ><div class="pageBoxInside" ><h2><a href="'.get_permalink().'">');
		the_title();
		echo('</a></h2><hr/>');
			
		the_excerpt();
		echo('<a href="'.get_permalink().'" class="moreLink">'.get_option('si_read_more').'</a>');
		echo('</div></div>');
	}
}else{
	echo('<p>'.get_option('_no_results_text').'</p>');
}

?>
</div>
<?php
get_sidebar('homeSidebar');
get_footer();   ?>
