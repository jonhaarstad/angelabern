</div>
</div>

<div id="footer">
  <div class="center">
    <p><?php echo(get_option('_footer_text')); ?></p>
    <div id="footerMenu">
     <?php if (function_exists('wp_nav_menu') && get_option('_enable_menus')=='on'){
        	wp_nav_menu(array('menu' => get_option('_footer_menu')));
        }else{?>
           <ul>
        	<?php wp_list_pages("depth=1&title_li=&exclude=".get_option("_exclude_pages_from_menu")); ?>
      		</ul>
        <?php 	
       }?>   
    </div>
  </div>
  <?php wp_footer(); ?>
</div>
</div>
</body></html>