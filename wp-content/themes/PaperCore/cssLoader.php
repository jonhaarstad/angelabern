<?php header("Content-type: text/css; charset: UTF-8"); ?>

<?php require_once( '../../../wp-load.php' );

$paper_css = array(
	'body_color' => get_option('_body_color'),
	'body_text_size' => get_option('_body_text_size'),
	'logo_image' => get_option('_logo_image'),
	'logo_width' => get_option('_logo_width'),
	'logo_height' => get_option('_logo_height'),
	'link_color' => get_option('_link_color'),
	'more_link_color' => get_option('_more_link_color'),
	'menu_link_color' => get_option('_menu_link_color'), 
	'menu_link_hover' => get_option('_menu_link_hover'),
	'menu_link_size' => get_option('_menu_link_size'),
	'footer_bg' => get_option('_footer_bg'),
	'footer_text_color' => get_option('_footer_text_color'),
	'sidebar_link_color' => get_option('_sidebar_link_color'), 
	'sidebar_link_hover' => get_option('_sidebar_link_hover'),
  	'heading_color' => get_option('_heading_color'),	 	
);
?>

body {
<?php if($paper_css['body_color']!=''){
	echo 'color:#'.$paper_css['body_color'].';';
}
if($paper_css['body_text_size']!=''){
	echo 'font-size:'.$paper_css['body_text_size'].'px;';
}?>
}

#logoContainer a{
<?php if($paper_css['logo_image']!=''){
	echo 'background-image:url('.$paper_css['logo_image'].');';
}
if($paper_css['logo_width']!=''){
	echo 'width:'.$paper_css['logo_width'].'px;';
}
if($paper_css['logo_height']!=''){
	echo 'height:'.$paper_css['logo_height'].'px;';
}?>
}

#logoContainer{
	<?php if($paper_css['logo_height']!='' && $paper_css['logo_height']>125){
	echo 'height:'.$paper_css['logo_height'].'px;';
}?>
}

a {
<?php if($paper_css['link_color']!=''){
	echo 'color:#'.$paper_css['link_color'].';';
}?>
}

.moreLink{
<?php if($paper_css['more_link_color']!=''){
	echo 'color:#'.$paper_css['more_link_color'].';';
}?>
}

#menu ul li a{
<?php if($paper_css['menu_link_color']!=''){
	echo 'color:#'.$paper_css['menu_link_color'].';';
}
if($paper_css['menu_link_size']!=''){
	echo 'font-size:'.$paper_css['menu_link_size'].'px;';
}
?>
}

#menu ul li a:hover{
<?php if($paper_css['menu_link_hover']!=''){
	echo 'color:#'.$paper_css['menu_link_hover'].';';
}?>
}

#footer{
<?php if($paper_css['footer_bg']!=''){
	echo 'background-color:#'.$paper_css['footer_bg'].';';
}?>
}

#footer p, #footerMenu ul li a{
<?php if($paper_css['footer_text_color']!=''){
	echo 'color:#'.$paper_css['footer_text_color'].';';
}?>
}

.sidebarMenu ul li a{
<?php if($paper_css['sidebar_link_color']!=''){
	echo 'color:#'.$paper_css['sidebar_link_color'].';';
}?>
}

.sidebarMenu ul li a:hover{
<?php if($paper_css['sidebar_link_hover']!=''){
	echo 'color:#'.$paper_css['sidebar_link_hover'].';';
}?>
}

h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
<?php if($paper_css['heading_color']!=''){
	echo 'color:#'.$paper_css['heading_color'].';';
}?>
}

<?php if(get_option('_additional_styles')!=''){
	echo(get_option('_additional_styles'));
}
?>

