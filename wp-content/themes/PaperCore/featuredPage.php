<?php
/*
Template Name: Featured posts page
*/

get_header();

if(have_posts()){
    while(have_posts()){
        the_post();
        $title=get_the_title();
        $pageId=get_the_ID();
        $subtitle=get_post_meta($pageId, 'subtitle', true);
        $catId=get_post_meta($pageId, 'postCategory', true);
  		$slider=get_post_meta($pageId, 'slider', true);
  		$postNumber=get_post_meta($pageId, 'postNumber', true);
    }
}

if($slider!='none' && $slider!=''){
	require_once $slider.'.php';
}?>

<div id="topText">
  <div class="hr3">
    <hr />
  </div>
  <p><?php echo(get_option('_top_text'));?></p>
  <div class="hr4">
    <hr />
  </div>
</div>
<div id="contentContainer">
<div id="content">
  <!--content-->
  <?php
   the_content();
   	?>
  <div class="featuredTitleContainer">
    <div class="hr1">
      <hr />
    </div>
    <?php echo($subtitle)?>
    <div class="hr2">
      <hr />
    </div>
  </div>
  <div class="postBoxs">
    <?php

if($postNumber==''){
	$postNumber=3;
}

	query_posts(array(
                'posts_per_page' => $postNumber, 
                'cat' => $catId
	));
	


	if(have_posts()){
		while(have_posts()){
			the_post();
			global $more;
			$more = 0;
			?>
    <div class="postBox">
      <div class="info">
        <div class="date"><span class="month">
          <?php the_time('M') ?>
          </span>
          <h1>
            <?php the_time('d') ?>
          </h1>
          <h3>
            <?php the_time('Y') ?>
          </h3>
        </div>
        <h1><a href="<?php the_permalink();?>">
          <?php the_title();?>
          </a></h1>
        <div class="postInfo">
          <?php the_category(',');?>
          | <?php echo get_option('_by_text'); ?>
          <?php the_author(); ?>
          | <a href="<?php the_permalink();?>#comments">
          <?php comments_number(get_option('_no_comments_text'), get_option('_one_comment_text'), '% '.get_option('_comments_text'))?>
          </a></div>
      </div>
      <div class="postContent">
       <?php print_post_thumbnail('postImg', $post); ?>
        <?php the_content('');?>
        <a href="<?php the_permalink(); ?>" class="moreLink"><?php echo(get_option("_read_more")); ?></a> </div>
    </div>
    <div class="hr6">
      <hr />
    </div>
    <?php
		}      
	}

?>
  </div>
</div>
<?php get_sidebar('homeSidebar'); ?>
<?php
   get_footer();
?>
