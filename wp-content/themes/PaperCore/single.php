<?php
get_header();

if(have_posts()){
	while(have_posts()){
		the_post();
		$id=get_the_ID();
		$cat=get_the_category();
		$catParent=$cat[0]->category_parent;
		 
		$nonBlogCats=explode(',',get_option('_exclude_cat_from_blog'));

		$blog=true;
		foreach ($cat as $c) {
			if(in_array($c->cat_ID,$nonBlogCats) || in_array($c->category_parent,$nonBlogCats)){
				$blog=false;
			}
		}

		$postTitle=get_the_title();
		 
		?>

<div id="contentContainer">
  <div id="pageHeader"></div>
  <div id="content">
    <?php


if($blog==true){

	?>
    <!--content-->
    <div class="postBox">
      <div class="info">
        <div class="date"><span class="month">
          <?php the_time('M') ?>
          </span>
          <h1>
            <?php the_time('d') ?>
          </h1>
          <h3>
            <?php the_time('Y') ?>
          </h3>
        </div>
        <h1><a href="<?php the_permalink();?>">
          <?php the_title();?>
          </a></h1>
        <div class="postInfo">
          <?php the_category(',');?>
          | <?php echo get_option('_by_text'); ?>
          <?php the_author(); ?>
          | <a href="<?php the_permalink();?>#comments">
          <?php comments_number(get_option('_no_comments_text'), get_option('_one_comment_text'), '% '.get_option('_comments_text'))?>
          </a></div>
      </div>
      <div class="postContent">
        <?php if(get_post_meta($post->ID, 'thumbnail', true)!=''){?>
        <img src="<?php echo get_post_meta($post->ID, 'thumbnail', true); ?>"
	alt="" class="postImg" />
        <?php } ?>
        <?php the_content('');?>
      </div>
    </div>
    <div id="comments">
      <?php comments_template(); ?>
    </div>
  </div>
  <?php

}else{   ?>
  <div class="postBoxContent">
    <h1>
      <?php the_title(); ?>
    </h1>
    <?php if(get_post_meta($post->ID, 'thumbnail', true)!=''){?>
    <img
	src="<?php echo get_post_meta($post->ID, 'thumbnail', true); ?>" alt=""
	class="postImg" />
    <?php } ?>
    <?php the_content();?>
  </div>
  <?php

if(get_option('_post_comments')==true){  ?>
  <div id="comments">
    <?php 

comments_template();  ?>
  </div>
  <?php } ?>
</div>
<?php    }
 
	}
}

if($blog==true){
	get_sidebar('blogSidebar');
}else{
	get_sidebar('homeSidebar');
}

?>
<?php get_footer();   ?>
