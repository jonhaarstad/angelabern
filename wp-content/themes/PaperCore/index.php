<?php
get_header();

$slider=get_option('_home_slider');

if($slider!='none'){
	require_once $slider.'.php';
}

if(have_posts()){
	while(have_posts()){
		the_post();
		$title=get_the_title();
	}
}


?>
<!--content-->
<div id="contentContainer">
<div id="content">
  <div class="postBoxs">
    <?php


$postsPerPage=get_option('_post_per_page_on_blog');
$excludeCat=explode(',',get_option('_exclude_cat_from_blog'));

query_posts(array(
                'category__not_in' => $excludeCat,
				'paged' => get_query_var('paged'),
			    'posts_per_page' => $postsPerPage
));



if(have_posts()){
	while(have_posts()){
		the_post();
		global $more;
		$more = 0;
		?>
    <div class="postBox">
      <div class="info">
        <div class="date"><span class="month">
          <?php the_time('M') ?>
          </span>
          <h1>
            <?php the_time('d') ?>
          </h1>
          <h3>
            <?php the_time('Y') ?>
          </h3>
        </div>
        <h1><a href="<?php the_permalink();?>">
          <?php the_title();?>
          </a></h1>
        <div class="postInfo">
          <?php the_category(',');?>
          | <?php echo get_option('_by_text'); ?>
          <?php the_author(); ?>
          | <a href="<?php the_permalink();?>#comments">
          <?php comments_number(get_option('_no_comments_text'), get_option('_one_comment_text'), '% '.get_option('_comments_text'))?>
          </a></div>
      </div>
      <div class="postContent">
        <?php print_post_thumbnail('postImg', $post); ?>
        <?php the_content('');?>
        <a
	href="<?php the_permalink(); ?>" class="moreLink"><?php echo(get_option("_read_more")); ?></a> </div>
    </div>
    <div class="hr6">
      <hr />
    </div>
    <?php
	}      ?>
    <div id="blogNavButtons" class="navigation">
      <div class="alignleft">
        <?php previous_posts_link('<span>&laquo;</span> '.get_option('_previous_text')) ?>
      </div>
      <div class="alignright">
        <?php next_posts_link(get_option('_next_text').' <span>&raquo;</span>') ?>
      </div>
    </div>
    <?php

}else{

}


?>
  </div>
</div>
<?php get_sidebar();
get_footer();
?>
