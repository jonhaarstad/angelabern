<div id="header">
  <div id="wrapper">
    <div class="fadeWrapper">
      <?php 

$separator='|*|';

$sliderImagesString = get_option('_fader_image_names');
$linkString=get_option('_fader_image_links');

$sliderImagesArray=explode($separator, $sliderImagesString);
$linkArray= explode($separator,$linkString);

$count=count($sliderImagesArray);
$linkCount=count($linkArray);

for($i=0;$i<$count-1;$i++){
	echo(' <div class="fadeHolder">');
	if($i<$linkCount){
		echo('<a href="'.$linkArray[$i].'">');
	}
	echo('<img src="');
	$path=$sliderImagesArray[$i];
	echo($path);
	echo('" alt=""/>');
	if($i<$linkCount){
		echo('</a>');
	}
	echo('</div>');
}
?>
    </div>
  </div>
</div>
