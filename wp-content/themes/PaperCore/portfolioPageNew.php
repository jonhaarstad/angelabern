<?php
/*
 Template Name: Portfolio page with descriptions
 */
get_header();

if(have_posts()){
	while(have_posts()){
		the_post();
		$pageId=get_the_ID();
		$pageTitle=get_the_title();
		$subtitle=get_post_meta($pageId, 'subtitle', true);
		$showCat=get_post_meta($pageId,'categories',true);
		$catId=get_post_meta($pageId,'postCategory',true);
		$postNumberToShow=get_post_meta($pageId,'postNumber',true);
		$titleLink=get_option('_title_link');
	}
}
?>

<div id="contentContainer">
  <div id="pageHeader"></div>
  <div id="portfolio">
    <?php 
if($showCat!='off'){

	?>
    <div id="portfolioCategories">
      <div class="leftBundle"></div>
      <span><?php echo(get_option('_categories_text')); ?></span>
      <ul>
        <li><a href="<?php echo(get_bloginfo('url').'/?page_id='.$pageId);?>">
          <?php if(!isset($_GET['cat'])){echo('<b>');}
	echo(get_option('_all_text'));
	if(!isset($_GET['cat'])){echo('</b>');} ?>
          </a></li>
        <?php

	$categories=  get_categories('child_of='.$catId);
	foreach ($categories as $cat) {
		echo('<li><a href="'.get_bloginfo("url").'/?page_id='.$pageId.'&cat='.$cat->cat_ID.'">');
		if(isset($_GET['cat']) && $_GET['cat']==$cat->cat_ID){echo('<b>');}
		echo($cat->cat_name);
		if(isset($_GET['cat']) && $_GET['cat']==$cat->cat_ID){echo('</b>');}
		echo('</a></li>');

	}

	?>
      </ul>
      <div class="rightBundle"></div>
    </div>
    <?php }?>
    <?php

if(isset($_GET['cat'])){
	$catId=$_GET['cat'];
}

query_posts(array(
                'posts_per_page' =>$postNumberToShow, 
                'paged' => get_query_var('paged'),
                'cat' => $catId
));

if ( have_posts() ) {
	$i=1;
	while ( have_posts() ){
		the_post();
		global $more;
		$more=0;
		$thumbnail=get_post_meta($post->ID, 'thumbnail', true);
		$preview=get_post_meta($post->ID, 'preview_image', true);
		$description=get_post_meta($post->ID, 'description', true);

		?>
		
		<?php if($i==1){?>
		<div class="portfolioItemsGroup">
		<?php }?>
		
		<div class="portfolioItem2">
		<h3><?php if($titleLink!='off'){?><a href="<?php the_permalink(); ?>"><?php } the_title();
            if($titleLink!='off'){ ?></a><?php }?></h3>
		
		<?php 
		if(get_option('_auto_portfolio_thumbnail')=='off'){
			//automatic thumbnail generation has been turned off 
			?>
			 <img src="<?php echo($thumbnail); ?>" alt=""/>
			<?php 
		}else{
			//automatic thumbnail generation is turned on  ?>
			<img src="<?php bloginfo("template_directory") ?>/script/timthumb.php?src=<?php echo($preview); ?>&amp;h=190&amp;w=290&amp;zc=1&amp;q=80" alt=""/>
		<?php
		}
		$class='';
		if(get_post_meta($post->ID, 'video', true)=='on'){
			$class=' video2';	
		}
		?>
		
		
		<p><?php echo($description); ?></p>
		
		   <div class="portfolioItemTop2"> 
		   <a rel="prettyPhoto[group]" class="single_image<?php echo($class); ?>" href="<?php echo($preview); ?>"></a>
           </div>
            
            
            </div>
            <?php if($i==3){?>
            </div>
            <?php } 
            if($i==3)$i=1;
            else $i++;
            ?>
		
  
  <?php        }
  if($i==2 || $i==3){?>
  	</div>
  <?php }
}

?>
  <div id="portfolioNavButtons" class="navigation">
    <div class="alignleft">
      <?php previous_posts_link('<span>&laquo;</span> '.get_option('_previous_text')) ?>
    </div>
    <div class="alignright">
      <?php next_posts_link(get_option('_next_text').' <span>&raquo;</span>') ?>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function(){
portfolioSetter.init();
});
</script>
<?php
get_footer();
?>
