<?php
/*
 Template Name: Contact form page
 */
get_header();

if(have_posts()){
	while(have_posts()){
		the_post();
		$pageId=get_the_ID();
		$subtitle=get_post_meta($pageId, 'subtitle', true);

		?>

<div id="contentContainer">
<div id="pageHeader"></div>
<!--content-->
<div id="content">
  <div id="pageContent">
    <?php 

the_content(); ?>
    <h2><?php echo(get_option('_ask_question_text'));?></h2>
    <?php
require_once ('form.php');
	}
}

?>
  </div>
</div>
<?php
get_sidebar('contactSidebar');
get_footer();   ?>
