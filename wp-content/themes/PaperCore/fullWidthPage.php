<?php
/*
 Template Name: Full width page
 */
get_header();



if(have_posts()){
	while(have_posts()){
		the_post();
		$pageId=get_the_ID();
		$title=get_the_title();
		$subtitle=get_post_meta($pageId, 'subtitle', true);

		?>

<div id="contentContainer">
<div id="pageHeader"></div>
<!--content-->
<div id="fullPageContent">
  <?php 

the_content();
	}
}

?>
</div>
<?php
get_footer();   ?>
