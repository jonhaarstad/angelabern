<div id="sidebar">
  <div class="sidebarBox">
    <?php     
    if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('blogSidebar')) { ?>
    To activate the sidebar you have to go to Appearance -> Widgets and drag and drop the desired widgets into "blogSidebar".
    <?php } ?>
  </div>
</div>

