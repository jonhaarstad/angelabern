<div id="sidebar">
  <div class="sidebarBox" role="complementary">
    <?php     
    if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('pageSidebar')) { ?>
    To activate the sidebar you have to go to Appearance -> Widgets and drag and drop the desired widgets into "pageSidebar".
    <?php } ?>
  </div>
</div>
