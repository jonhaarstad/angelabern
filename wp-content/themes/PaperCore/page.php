<?php
get_header();

if(have_posts()){
	while(have_posts()){
		the_post();
		$pageId=get_the_ID();
		$subtitle=get_post_meta($pageId, 'subtitle', true);
		$slider=get_post_meta($pageId, 'slider', true);


		if($slider!='none' && $slider!=''){
			require_once $slider.'.php';
	 }
	 ?>

<div id="contentContainer">
<!--content-->
<div id="content">
  <div id="pageContent">
    <?php 

the_content();
	}
}

?>
  </div>
</div>
<?php
get_sidebar('pageSidebar');
get_footer();
?>
