<?php
/*
 Template Name: Home page
 */

get_header();

if(have_posts()){
	while(have_posts()){
		the_post();
		$title=get_the_title();
		$pageId=get_the_ID();
		$subtitle=get_post_meta($pageId, 'subtitle', true);
		$catId=get_post_meta($pageId, 'postCategory', true);
		$slider=get_post_meta($pageId, 'slider', true);
		 
	}
}

if($slider!='none' && $slider!=''){
	require_once $slider.'.php';
}?>

<div id="topText">
  <div class="hr3">
    <hr />
  </div>
  <p><?php echo(get_option('_top_text'));?></p>
  <div class="hr4">
    <hr />
  </div>
</div>
<div id="contentContainer">
<div id="content">
  <!--content-->
  <?php
the_content();


?>
  <div class="featuredTitleContainer">
    <div class="hr1">
      <hr />
    </div>
    <?php echo($subtitle)?>
    <div class="hr2">
      <hr />
    </div>
  </div>
  <div class="servicesBoxs">
    <?php

if($catId!=''){
	query_posts(array(
                'posts_per_page' => -1, 
                'cat' => $catId,
	));


	if(have_posts()){
		while(have_posts()){
			the_post();
			global $more;
			$more = 0;
			?>
    <div class="servicesBox">
     <?php print_post_thumbnail('postImg', $post); ?>
      <h2><a href="<?php the_permalink(); ?>">
        <?php the_title(); ?>
        </a></h2>
      <?php the_content(''); ?>
    </div>
   <div class="hr6">
      <hr />
    </div>
    <?php
		}      ?>
    <?php

	}else{
			
	}
}

?>
  </div>
</div>
<?php get_sidebar('homeSidebar'); ?>
<?php
get_footer();
?>
