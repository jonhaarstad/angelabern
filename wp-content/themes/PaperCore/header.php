<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
<?php language_attributes() ?>>
<head>
<meta http-equiv="Content-Type"
	content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title>
<?php if (is_home()) {
	echo bloginfo('name');
} elseif (is_category()) {
	echo __('Category &raquo; ', 'blank'); wp_title('&laquo; @ ', TRUE, 'right');
	echo bloginfo('name');
} elseif (is_tag()) {
	echo __('Tag &raquo; ', 'blank'); wp_title('&laquo; @ ', TRUE, 'right');
	echo bloginfo('name');
} elseif (is_search()) {
	echo __('Search results &raquo; ', 'blank');
	echo the_search_query();
	echo '&laquo; @ ';
	echo bloginfo('name');
} elseif (is_404()) {
	echo '404 '; wp_title(' @ ', TRUE, 'right');
	echo bloginfo('name');
} else {
	echo wp_title(' @ ', TRUE, 'right');
	echo bloginfo('name');
} ?>
</title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"
	type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml"
	title="<?php bloginfo('name'); ?> RSS"
	href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="stylesheet"
	href="<?php bloginfo('template_directory'); ?>/prettyPhoto.css"
	type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet"
	href="<?php bloginfo('template_directory'); ?>/superfish.css"
	type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/jquery-1.4.js"></script>
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/script.js"></script>
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/superfish.js"></script>
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/fader.js"></script>
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/slider.js"></script>
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/selector.js"></script>
	<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/selector-new.js"></script>
<script type="text/javascript"
	src="<?php bloginfo('template_directory'); ?>/script/jquery.prettyPhoto.js"></script>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<!-- enables nested comments in WP 2.7 -->
<?php

$style=get_option('_style');
if($style!='original' && style!=''){
	echo('<link href="'.get_bloginfo('template_directory').'/style_'.$style.'.css" rel="stylesheet" type="text/css" />');
}

?>
<link rel="stylesheet"
	href="<?php bloginfo('template_directory'); ?>/cssLoader.php"
	type="text/css" media="screen" charset="utf-8" />

<script type="text/javascript" charset="utf-8">

		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'light_rounded' /* light_rounded / dark_rounded / light_square / dark_square */
			});
		});
	</script>
<!--[if lte IE 6]>
<link href="<?php bloginfo('template_directory'); ?>/style_ie6.css" rel="stylesheet" type="text/css" />  
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/script/supersleight.js"></script>
<?php if($trans==true){?>
<style type="text/css" media="screen">
#contentContainer{background-image:url(<?php echo(get_bloginfo('template_directory').'/images/'.$img);?>.jpg);	background-position: 2px;}
<?php if($style=='trans' || $style=='trans2'){?>
	#navButtonLeft a{
	background-image:url(<?php bloginfo('template_directory'); ?>/images/nav_buttons_trans.jpg);
	}
	#navButtonRight a{
	background-image:url(<?php bloginfo('template_directory'); ?>/images/nav_buttons_trans.jpg);
	}
	#contentBottom{
	background-image:url(<?php bloginfo('template_directory'); ?>/images/bottom_trans.jpg);
	}
<?php }?>
</style>
<?php }?>
<![endif]-->
<!--[if IE 7]>
<link href="<?php bloginfo('template_directory'); ?>/style_ie7.css" rel="stylesheet" type="text/css" />  
<![endif]-->
<?php wp_head(); //leave for plugins ?>
</head>
<body>
<div id="mainContainer">
<div id="lineTop"></div>
<div class="center">
<div id="logoContainer"><a href="<?php bloginfo('url');?>"></a></div>
<div id="navigationContainer">
  <div class="hr1">
    <hr />
  </div>
  <!--menu-->
  <div id="menu">
    <?php if (function_exists('wp_nav_menu') && get_option('_enable_menus')=='on'){
        	wp_nav_menu(array('menu' => get_option('_top_menu')));
        }else{?>
           <ul class="sf-menu">
	      	<?php wp_list_pages("title_li=&exclude=".get_option("_exclude_pages_from_menu")); ?>
	    	</ul>
        <?php 	
       }?>    
  </div>
  <!--End menu-->
  <div class="hr2">
    <hr />
  </div>
</div>
