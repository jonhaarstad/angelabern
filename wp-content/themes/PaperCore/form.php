<form action="<?php bloginfo('template_directory')?>/sendEmail.php" method="post" id="submitForm">
  <label id="message"><?php echo(get_option('_message_sent_text'));?></label>
  <span
    class="greyTitle smallerText"><?php echo(get_option('_name_text'));?></span> <span id="nameError"
    class="errorMessage"><?php echo(get_option('_name_error_text'));?></span><br />
  <input type="text" name="nameTextBox" class="input" id="nameTextBox" />
  <br />
  <br />
  <span class="greyTitle smallerText"><?php echo(get_option('_your_email_text'));?></span> <span
    id="emailError" class="errorMessage"><?php echo(get_option('_email_error_text'));?></span><br />
  <input type="text" name="emailTextBox" class="input" id="emailTextBox" />
  <br />
  <br />
  <span class="greyTitle smallerText"><?php echo(get_option('_question_text'));?></span> <span
    id="questionError" class="errorMessage"><?php echo(get_option('_question_error_text'));?></span><br />
  <textarea name="questionTextArea" rows="" cols="" class="textArea input"
    id="questionTextArea"></textarea>
  <br />
  <br />
  <input type="button" value="<?php echo(get_option('_send_text'));?>" id="sendButton" />
  <?php $url=get_bloginfo('template_directory').'/sendEmail.php'; ?>
  <input type="hidden" value="<?php echo($url); ?>" name="url" id="url" />
</form>
