﻿/**
 * This is the portfolio category selector. By defined portfolio categories when clicking
 * on a specified category, shows only the items that belong to this category. Also sets the
 * info that is shown when hovering a portfolio item.
 * 
 * Author: Pexeto
 * http://pexeto.com/
 */


var portfolioSetter={
	imgWidth:300,
	imgHeight:200,
	imgArray:new Array(),
	imgNumber:0,
	
	/**
	 * Inits the portfolio setter. Calls the main functions for setting category selectors and
	 * portfolio items info.
	 */
	init:function(){
		this.getAllDivs();
		if(this.imgNumber>0){
			this.setInfo('.portfolioItemTop');
		}
		this.setMainPortfolioHover();
	},
	
	/**
	 *	Gets all the divs that have to be shown in the slider and fills them in an array.
	 */
	getAllDivs: function(){
		//fill the divs in an array
		$("#portfolioItem2 img").each(function(i){
			portfolioSetter.imgArray[i]=$(this);
			portfolioSetter.imgNumber++;
		});
	},
	
	
	/**
	 * Sets the info for the item when it is hovered.
	 */
	setInfo: function($selector){		
		$($selector).each(function(){
			
			$(this).hover(function(){
				$(this).stop().animate({opacity:0.7});
				$(this).find('a').animate({opacity:0.9}).show();
			},function(){
				$(this).stop().animate({opacity:0});
				$(this).find('a').hide();
			});
		
		});
	},
	
	setMainPortfolioHover:function(){
		$('.portfolioItemTop2').show().animate({opacity:0},0);
		$('.portfolioItemTop2 a').hide();
		this.setInfo('.portfolioItemTop2');
	}
		
};
