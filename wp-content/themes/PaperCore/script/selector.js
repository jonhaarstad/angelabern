﻿/**
 * This is the portfolio category selector. By defined portfolio categories when
 * clicking on a specified category, shows only the items that belong to this
 * category.
 * 
 * Author: Pexeto 
 * http://pexeto.com/
 */

var imgArray = new Array();
var imgNumber = 0;

var imgWidth = 270;
var imgHeight = 170;

var timer;

$(function() {
	getAllDivs();
	setHover();


	$("#portfolioCategories span:first").css( {
		fontWeight : "bold"
	});

	setInfo();

});

/**
 * Gets all the divs that have to be shown in the slider and fills them in an
 * array.
 */
function getAllDivs() {
	// fill the divs in an array
	$("#portfolio img").each(function(i) {
		imgArray[i] = $(this);
		imgNumber++;
	});

}

/**
 * Makes the cursor to pointer when the mouse is positioned over a category name.
 * @return
 */
function setHover() {
	$("#portfolioCategories span").mouseover(function() {
		$(this).css( {
			cursor : "pointer"
		});
	});
}

/**
 * Sets the info functionality for the portfolio items when an item is hovered.
 * 
 * @return
 */
function setInfo() {
	
	//position the info for the first time
	$("#portfolio div.portfolioItem").each(function(i) {
		var info = $(this).find("div.portfolioItemInfo");
		var nDivHeight = info.outerHeight(true);
		var nDivPlacement = nDivHeight - (2 * nDivHeight);
		info.css( {
			bottom : nDivPlacement - 20
		});
	});

	//set the hover functionality
	$("#portfolio div.portfolioItem").hover(function() {
		var info = $(this).find("div.portfolioItemInfo");
		info.stop().animate( {
			bottom : "4px"
		}, 500);
	}, function() {
		var info = $(this).find("div.portfolioItemInfo");

		var nDivHeight = info.outerHeight(true);
		var nDivPlacement = nDivHeight - (2 * nDivHeight);
		info.stop().animate( {
			bottom : nDivPlacement - 20
		}, 500);

	});
}