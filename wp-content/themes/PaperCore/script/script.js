﻿/**
 * This is the main JS file. It initializes some plugins and contains the
 * functionality for the send email form.
 * 
 * Author: Pexeto http://pexeto.com/
 */

var searchClicked = false;
var valid = true;

$(function() {
	$('#menu ul').addClass('sf-menu');
	$('ul.sf-menu').superfish();
	validateSendEmailForm();
	checkIfOpera();
	
	$('.sf-sub-indicator').each(function(){
		$(this).siblings('.sf-sub-indicator').hide();
		$(this).show();
	});
});

/**
 * Validates the send email form.
 */
function validateSendEmailForm() {
	$("#sendButton")
			.click(function() {

				// clear previous messages
					$("#nameError").hide();
					$("#emailError").hide();
					$("#questionError").hide();
					valid = true;

					// verify whether the name text box is empty
					if (document.getElementById("nameTextBox").value == ""
							|| document.getElementById("nameTextBox").value == null) {
						$("#nameError").show();
						valid = false;
					}

					// verify whether the question text area is empty
					if (document.getElementById("questionTextArea").value == ""
							|| document.getElementById("questionTextArea").value == null) {
						$("#questionError").show();
						valid = false;
					}

					// verify whether the inserted email address is valid
					var email = document.getElementById("emailTextBox").value;
					if (!isValidEmailAddress(email)) {
						$("#emailError").show();
						valid = false;
					}

					// verify whether the email text box is empty
					if (document.getElementById("emailTextBox").value == ""
							|| document.getElementById("emailTextBox").value == null) {
						$("#emailError").show();
						valid = false;
					}

					var name = document.getElementById("nameTextBox").value;
					var question = document.getElementById("questionTextArea").value;

					// if the inserted data is valid, then sumbit the form
					if (valid == true) {
						urlToPhp = document.getElementById("url").value;

						//create the data string for the AJAX request
						var dataString = 'name=' + name + '&question='
								+ question + '&email=' + email;

						//execute the AJAX request
						$.ajax( {
							type : "POST",
							url : urlToPhp,
							data : dataString,
							success : function() {
								$("label#message").show();
								$("label#message").append("<br/><br/>");
								$("#submitForm").each(function() {
									this.reset();
								});
							}
						});
					}
				});
}

/**
 * Positions the dropdown children of the menu.
 */
function positionUlChildren() {
	$("#menu ul li").each(function(i) {
		var childUl = $(this).find("ul");
		var left = $(this).find("a").offset().left - $("#menu").offset().left;
		childUl.css( {
			left : left
		});

		childUl.hover(function() {
			$(this).parent("li").find("a").addClass("selected");
		}, function() {
			$(this).parent("li").find("a").removeClass("selected");
		});
	});

}

/**
 * Checks if the current browser is Opera and if it is, fixes a drop-down menu
 * issue for this browser.
 * 
 * @return
 */
function checkIfOpera() {
	if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
		$(".sf-menu a").css( {
			display : "block"
		});
		$(".sf-menu li").css( {
			"float" : "left",
			top : "-10px"
		});
		$(".sf-menu").css( {
			marginLeft : "80px"
		});
		$(".sf-menu li ul").css( {
			top : "32px",
			paddingTop : "10px"
		});
		$("#menu ul li ul li ul").css( {
			top : "0px"
		});
	}
}

/**
 * Checks if an email address is a valid one.
 * 
 * @param emailAddress
 *            the email address to validate
 * @return true if the address is a valid one
 */
function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}

