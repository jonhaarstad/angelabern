﻿/**
 * This file contains the functionality for the image fader on the home page.
 * 
 * Author: Pexeto
 * http://pexeto.com/
 */


var fadeDivArray = new Array();
var navArray = new Array();

var fadeDivNumber=0;
var currentFadeImage=0;
var selectedFadeImage=0;

var fadeWaitInterval=5000;  //this is the interval between each fade
var fadeSpeed=2000;    //this is the speed of the fade action
var selectFadeSpeed=1000;


var timer=-1;

$(function(){
getAllFadeDivs();

if(fadeDivNumber>0){
	setFader();
	setClickHandlers();
	setArrowClickHandlers();
	timer = window.setInterval("fade()", fadeWaitInterval);
	setLinks();
}
});

function setLinks(){
	$(".fadeWrapper").click(function(){
		var link=$(".fadeHolder").eq(currentFadeImage).find("a").attr("href");
		if(link!=null){
			location.href=link;
		}
	});
	
	$(".fadeWrapper").mouseover(function(){
		$(".fadeWrapper").css({cursor:"pointer"});
	});
}

/**
 *	Gets all the divs that have to be shown in the slider and fills them in an array.
 */
function getAllFadeDivs(){

$("#sliderNavigation").append("<ul class=\"navUl\"></ul>");

	//fill the divs in an array
	$(".fadeHolder").each(function(i){
		fadeDivArray[i]=$(this);
		fadeDivNumber++;
		if(i!=0){
			$(".navUl").append("<li><a href=\"#\"></a></li>");
		}else{
			$(".navUl").append("<li class=\"selected\"><a href=\"#\"></a></li>");
		}
		
		navArray[i]=$("ul.navUl li").eq(i);
	});
}

/**
 *	Makes all the images invisible.
 */
function setFader(){
	for(var i=1; i<fadeDivNumber; i++){
		fadeDivArray[i].find("img").css({display:"none"});
	}
}

function setClickHandlers(){
	$("ul.navUl li").each(function(i){	
		$(this).click(function(){
			window.clearInterval(timer);
			selectedFadeImage=i;
			fadeSelected();	
			timer = window.setInterval("fade()", fadeWaitInterval);
		});		
	});
}

function setArrowClickHandlers(){
	$("#leftArrow").click(function(){
		if(currentFadeImage!=0){
			window.clearInterval(timer);
			selectedFadeImage=currentFadeImage-1;
			fadeSelected();	
			timer = window.setInterval("fade()", fadeWaitInterval);
		}
	});
	
	$("#rightArrow").click(function(){
		if(currentFadeImage!=fadeDivNumber-1){
			window.clearInterval(timer);
			selectedFadeImage=currentFadeImage+1;
			fadeSelected();	
			timer = window.setInterval("fade()", fadeWaitInterval);
		}
	});
}

function fadeSelected(){
	var img=fadeDivArray[currentFadeImage].find("img");
	img.fadeOut(selectFadeSpeed);
	
	var navLi=navArray[currentFadeImage];
	navLi.removeClass("selected");
			
	img=fadeDivArray[selectedFadeImage].find("img");
	img.fadeIn(selectFadeSpeed);
	
	navLi=navArray[selectedFadeImage];
	navLi.addClass("selected");
	
	currentFadeImage=selectedFadeImage;
}

/**
 *	The whole fading is performed here.
 */
function fade(){
	var img=fadeDivArray[currentFadeImage].find("img");
	img.fadeOut(fadeSpeed);
	
	var navLi=navArray[currentFadeImage];
	navLi.removeClass("selected");
	
	if(currentFadeImage<fadeDivNumber-1){
		img=fadeDivArray[currentFadeImage+1].find("img");
		navLi=navArray[currentFadeImage+1];
		currentFadeImage++;
	}else{
		img=fadeDivArray[0].find("img");
		navLi=navArray[0];
		currentFadeImage=0;
	}
		
	img.fadeIn(fadeSpeed);
	navLi.addClass("selected");
}
