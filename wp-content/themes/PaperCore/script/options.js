﻿/**
 * This is the JS file for the admin options page.
 * 
 * Author: Pexeto http://pexeto.com/
 */

var separator = '|*|';

$(function() {
	setCheckboxClickHandlers();

	$(".sortable").sortable();
	$(".sortable").disableSelection();
	
	//load the color picker for changing theme style
	$('input.color').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});

});

/**
 * Sets a checkbox click handler. If a checkbox is checked, then the element is
 * added. If the checkbox is unchecked, then the element is removed.
 * 
 * @return
 */
function setCheckboxClickHandlers() {
	$(".check").click(function() {
		var value = this.value;
		var checked = false;
		if ($(this).is(':checked')) {
			checked = true;
		}

		var idsString = $(this).siblings(".hiddenText").val();

		if (checked == true) {
			// it is checked, add the element to the list
			if (idsString != "") {
				idsString += "," + value;
			} else {
				idsString += value;
			}
		} else {
			// it is unchecked, remove the element from thr list
			var idsArray = idsString.split(",");
			idsString = "";
			for ( var i = 0; i < idsArray.length; i++) {
				if (idsArray[i] != value) {
					if (i != idsArray.length) {
						idsString += idsArray[i] + ",";
					} else {
						idsString += idsArray[i];
					}
				}
			}

			var strLen = idsString.length;
			var lastChar = idsString.charAt(strLen - 1);
			if (lastChar == ',') {
				idsString = idsString.slice(0, strLen - 1);
			}

		}

		$(this).siblings(".hiddenText").val(idsString);

	});
}

/**
 * Shows the already saved data
 * 
 * @param ulId
 *            the ID of the ul element that contains the data
 * @param inputIds
 *            the IDs of the inputs to insert the data sepearated by commas
 * @param spanClasses
 *            the classes of the spans that show the data separated by commas
 * @param hiddenIds
 *            the IDs of the hidden inputs that contain all the data separated
 *            by commas
 * @param labels
 *            the IDs of the labels to output the data
 * @return
 */
function showSavedImgData(ulId, inputIds, spanClasses, hiddenIds, labels) {

	var count = inputIds.length;
	var data = new Array();
	for ( var i = 0; i < count; i++) {
		data[i] = $(hiddenIds[i]).attr("value").split(separator);
	}

	var entryCount = data[0].length;
	// goes through all the saved entries and outputs the saved data
	for ( var j = 0; j < entryCount - 1; j++) {
		var html = '<li>';
		for ( var i = 0; i < count; i++) {
			html += '<b>' + labels[i] + ': </b><span class="' + spanClasses[i]
					+ '">' + data[i][j] + '</span><br/>';
		}
		html += '<div class="deleteButton"></li>';
		$(ulId).append(html);
	}

}

/**
 * Initializes the functionality for adding/removing slider images
 * 
 * @return
 */
function setSliderFunc() {
	var ulId = '#sortable';
	var inputIds = [ '#urlImg', '#titleImg', '#linkImg', '#descImg' ];
	var spanClasses = [ 'url', 'title', 'link', 'desc' ];
	var hiddenIds = [ '#_url_img', '#_title_img', '#_link_img', '#_desc_img' ];
	var labels = [ 'Image URL', 'Image Title', 'Title Link', 'Description' ];
	var addButton = '#addImageButton';

	setCommonAddFunc(ulId, inputIds, spanClasses, hiddenIds, labels, addButton)
}

/**
 * Initializes the functionality for adding/removing fader images
 * 
 * @return
 */
function setFaderFunc() {
	var ulId = '#sortableFade';
	var inputIds = [ '#urlFadeImg', '#linkFadeImg' ];
	var spanClasses = [ 'fadeUrl', 'fadeLink' ];
	var hiddenIds = [ '#_fader_image_names', '#_fader_image_links' ];
	var labels = [ 'Image URL', 'Image Link' ];
	var addButton = '#addFaderImageButton';

	setCommonAddFunc(ulId, inputIds, spanClasses, hiddenIds, labels, addButton)
}

/**
 * Calls the main functions that execute the functionality.
 * 
 * @param ulId
 *            the ID of the ul element that contains the data
 * @param inputIds
 *            the IDs of the inputs to insert the data sepearated by commas
 * @param spanClasses
 *            the classes of the spans that show the data separated by commas
 * @param hiddenIds
 *            the IDs of the hidden inputs that contain all the data separated
 *            by commas
 * @param labels
 *            the IDs of the labels to output the data
 * @param addButton
 *            the button whose click event will be handled
 * 
 * @return
 */
function setCommonAddFunc(ulId, inputIds, spanClasses, hiddenIds, labels,
		addButton) {

	showSavedImgData(ulId, inputIds, spanClasses, hiddenIds, labels);
	$(addButton).click(function() {
		addItem(ulId, inputIds, spanClasses, hiddenIds, labels);
	});

	$(ulId).bind('sortstop', function(event, ui) {
		setSliderImgChanges(ulId, inputIds, spanClasses, hiddenIds, labels);
	});

	setSortableHandlers(ulId);
	setDeleteButtonHandlers(ulId, inputIds, spanClasses, hiddenIds, labels);
}

/**
 * 
 * Adds a new item to the list.
 * 
 * @param ulId
 *            the ID of the ul element that contains the data
 * @param inputIds
 *            the IDs of the inputs to insert the data sepearated by commas
 * @param spanClasses
 *            the classes of the spans that show the data separated by commas
 * @param hiddenIds
 *            the IDs of the hidden inputs that contain all the data separated
 *            by commas
 * @param labels
 *            the IDs of the labels to output the data
 * @return
 */
function addItem(ulId, inputIds, spanClasses, hiddenIds, labels) {

	var length = inputIds.length;

	var html = '<li>';
	for ( var i = 0; i < length; i++) {
		html += '<b>' + labels[i] + ': </b><span class="' + spanClasses[i]
				+ '">' + $(inputIds[i]).attr("value") + '</span><br/>';
	}
	html += '<div class="deleteButton"></li>';

	$(ulId).append(html);
	setSliderImgChanges(ulId, inputIds, spanClasses, hiddenIds, labels);

}

/**
 * Refreshes the output data after an item has been added/removed.
 * 
 * @param ulId
 *            the ID of the ul element that contains the data
 * @param inputIds
 *            the IDs of the inputs to insert the data sepearated by commas
 * @param spanClasses
 *            the classes of the spans that show the data separated by commas
 * @param hiddenIds
 *            the IDs of the hidden inputs that contain all the data separated
 *            by commas
 * @param labels
 *            the IDs of the labels to output the data
 * @return
 */
function setSliderImgChanges(ulId, inputIds, spanClasses, hiddenIds, labels) {

	var count = inputIds.length;
	var values = new Array();

	for (i = 0; i < count; i++) {
		values[i] = '';
	}

	$(ulId + ' li').each(
			function() {
				for (i = 0; i < count; i++) {
					values[i] += $(this).find('span.' + spanClasses[i]).text()
							+ separator;
				}
			});

	for (i = 0; i < count; i++) {
		$(hiddenIds[i]).attr("value", values[i]);
	}

	setSortableHandlers(ulId);
	setDeleteButtonHandlers(ulId, inputIds, spanClasses, hiddenIds, labels);
}

/**
 * Sets the sortable handler for the lists.
 * 
 * @param ulId
 *            the ID of the ul whose li children will be sorted
 * @return
 */
function setSortableHandlers(ulId) {
	$(ulId + ' li').hover(function() {
		$(this).css( {
			cursor : "move",
			backgroundColor : "#fff"
		});
	}, function() {
		$(this).css( {
			backgroundColor : "#f2f2f2"
		});
	});

}

/**
 * 
 * Sets the delete buttons click event handlers.
 * 
 * @param ulId
 *            the ID of the ul element that contains the data
 * @param inputIds
 *            the IDs of the inputs to insert the data sepearated by commas
 * @param spanClasses
 *            the classes of the spans that show the data separated by commas
 * @param hiddenIds
 *            the IDs of the hidden inputs that contain all the data separated
 *            by commas
 * @param labels
 *            the IDs of the labels to output the data
 * @return
 */
function setDeleteButtonHandlers(ulId, inputIds, spanClasses, hiddenIds, labels) {
	$('.deleteButton').mouseover(function() {
		$(this).css( {
			cursor : "pointer"
		});
	});

	$('.deleteButton').click(function() {
		$(this).parent("li").remove();
		setSliderImgChanges(ulId, inputIds, spanClasses, hiddenIds, labels);
	});
}