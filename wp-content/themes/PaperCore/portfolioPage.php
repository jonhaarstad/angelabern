<?php
/*
 Template Name: Portfolio page
 */
get_header();

if(have_posts()){
	while(have_posts()){
		the_post();
		$pageId=get_the_ID();
		$pageTitle=get_the_title();
		$subtitle=get_post_meta($pageId, 'subtitle', true);
		$showCat=get_post_meta($pageId,'categories',true);
		$catId=get_post_meta($pageId,'postCategory',true);
		$postNumberToShow=get_post_meta($pageId,'postNumber',true);
	}
}
?>

<div id="contentContainer">
  <div id="pageHeader"></div>
  <div id="portfolio">
    <?php 
if($showCat!='off'){

	?>
    <div id="portfolioCategories">
      <div class="leftBundle"></div>
      <span><?php echo(get_option('_categories_text')); ?></span>
      <ul>
        <li><a href="<?php echo(get_bloginfo('url').'/?page_id='.$pageId);?>">
          <?php if(!isset($_GET['cat'])){echo('<b>');}
	echo(get_option('_all_text'));
	if(!isset($_GET['cat'])){echo('</b>');} ?>
          </a></li>
        <?php

	$categories=  get_categories('child_of='.$catId);
	foreach ($categories as $cat) {
		echo('<li><a href="'.get_bloginfo("url").'/?page_id='.$pageId.'&cat='.$cat->cat_ID.'">');
		if(isset($_GET['cat']) && $_GET['cat']==$cat->cat_ID){echo('<b>');}
		echo($cat->cat_name);
		if(isset($_GET['cat']) && $_GET['cat']==$cat->cat_ID){echo('</b>');}
		echo('</a></li>');

	}

	?>
      </ul>
      <div class="rightBundle"></div>
    </div>
    <?php }?>
    <?php

if(isset($_GET['cat'])){
	$catId=$_GET['cat'];
}

query_posts(array(
                'posts_per_page' =>$postNumberToShow, 
                'paged' => get_query_var('paged'),
                'cat' => $catId
));

if ( have_posts() ) {
	while ( have_posts() ){
		the_post();
		echo('<div class="portfolioItem" >');


		$thumbnail=get_post_meta($post->ID, 'thumbnail', true);
		$preview=get_post_meta($post->ID, 'preview_image', true);
		$description=get_post_meta($post->ID, 'description', true);
		$video=get_post_meta($post->ID, 'video', true);


		if(get_option('_auto_portfolio_thumbnail')=='off'){
			//automatic thumbnail generation has been turned off
			echo('<a rel="prettyPhoto[group]" class="single_image" href="'.$preview.'">');

			echo('<img src="'.$thumbnail.'" alt=""/>');
			if($video=='on'){
				echo('<div class="video"></div>');
			}
			echo('</a>');
		}else{
			//automatic thumbnail generation is turned on
			echo('<a rel="prettyPhoto[group]" class="single_image" href="'.$preview.'">');

			echo('<img src="'.get_bloginfo("template_directory").'/script/timthumb.php?src='.$preview.'&amp;h=190&amp;w=290&amp;zc=1&amp;q=80" alt=""/>');
			if($video=='on'){
				echo('<div class="video"></div>');
			}
			echo('</a>');
		}
		?>
    <div class="portfolioItemInfo">
      <p class="infoTitle">
        <?php if(get_option('_title_link')!='off'){?>
        <a
	href="<?php the_permalink();?>">
        <?php }?>
        <?php the_title();?>
        <?php if(get_option('_title_link')!='off'){?>
        </a>
        <?php }?>
      </p>
      <p><?php echo($description);?></p>
    </div>
  </div>
  <?php        }
}

?>
  <div id="portfolioNavButtons" class="navigation">
    <div class="alignleft">
      <?php previous_posts_link('<span>&laquo;</span> '.get_option('_previous_text')) ?>
    </div>
    <div class="alignright">
      <?php next_posts_link(get_option('_next_text').' <span>&raquo;</span>') ?>
    </div>
  </div>
</div>
<?php
get_footer();
?>
