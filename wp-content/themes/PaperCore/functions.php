<?php
$categories=get_categories('hide_empty=0');
for($i=0; $i<count($categories); $i++){
	$cat_ids[$i]=$categories[$i]->cat_ID;
	$cat_names[$i]=$categories[$i]->cat_name;
}

$pages=get_pages();
for($i=0; $i<count($pages); $i++){
	$page_ids[$i]=$pages[$i]->ID;
	$page_names[$i]=$pages[$i]->post_title;
}

$tags=get_tags();
for($i=0; $i<count($tags); $i++){
	$tag_ids[$i]=$tags[$i]->term_id;
	$tag_names[$i]=$tags[$i]->name;
}


if (function_exists('add_theme_support')) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 200, 200, true );
}

/*-------------------- CALL THE FUNCTIONS FOR CREATING CUSTOM FIELDS  --------------------*/

add_action('admin_menu', 'create_meta_box');
add_action('admin_menu', 'create_meta_post_box');
add_action('save_post', 'save_postdata');
add_action('save_post', 'save_post_postdata');


$themename = "PaperCore";
$shortname = "";
$options = array (

array(
"name" => "You can set your theme options here.",
"type" => "misc"),

//------------------MAIN SETTINGS-------------------

array(
"name" => "Main settings",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Theme style",
"id" => $shortname."_style",
"type" => "select",
"options" => array("original(white)", "dark", "grunge", "note paper"),
"values" => array("original", "dark", "grunge", "paper"),
"std" => "original"
),

array(
"name" => "Slider on posts/blog page",
"id" => $shortname."_home_slider",
"type" => "select",
"options" => array('Fader', 'Accordion Slider', 'None'),
"values" => array('fader', 'slider', 'none'),
"std" => 'fader'
),

array(
"name" => "Header text",
"desc" => "The text that will be shown on the home page templates",
"id" => $shortname."_top_text",
"std" => "This is The Paper Core, an unique elegant and minimal Themeforest template and it is here to present your business and work in the best way.",
"type" => "text"
),

array(
"name" => "Footer text",
"desc" => "The text that will be shown on the footer such as copyright text",
"id" => $shortname."_footer_text",
"std" => "PaperCore by Pexeto",
"type" => "text"
),

array(
"type" => "close"),

array(
"type" => "submit"),



//------------------MENUS-------------------

array(
"name" => "Menus",
"type" => "title"),


array(
"type" => "open"),

array(
"name" => "Enable WordPress 3.0 menu support",
"id" => $shortname."_enable_menus",
"type" => "select",
"options" => array('Off', 'On'),
"values" => array('off', 'on'),
"std" => 'off',
"desc" => 'WordPress 3.0 provides a way to more easily control your menu links. If this option is set to off,
then only pages will be displayed in the menu. For more reference please read in the documentation the section
header -> the menu.'
),

array(
"name" => "Header menu name",
"desc" => "The name of the main top menu. Set this field only
if you are using WP 3.0 or higher and the field above is set to On.",
"id" => $shortname."_top_menu",
"type" => "text"
),

array(
"name" => "Footer menu name",
"desc" => "The name of the menu that will be shown in the footer. Set this field only
if you are using WP 3.0 or higher and the field above is set to On.",
"id" => $shortname."_footer_menu",
"type" => "text"
),

array(
"name" => "Exclude pages from menu - for older WP versions",
"id" => $shortname."_exclude_pages_from_menu",
"type" => "multicheck",
"options" => $page_names,
"values" => $page_ids,
"desc" => "Use this only when the field above is set to Off"),


array(
"type" => "close"),

array(
"type" => "submit"),



//------------------STYLES-------------------

array(
"name" => "Styles- you can change the default styles",
"type" => "title"),


array(
"type" => "open"),

array(
"name" => "Main body text color",
"id" => $shortname."_body_color",
"type" => "text",
"color" => true
),

array(
"name" => "Main body text size",
"id" => $shortname."_body_text_size",
"type" => "text",
"desc" => "The main body font size in pixels. Default: 17"
),

array(
"name" => "Logo image path",
"id" => $shortname."_logo_image",
"type" => "text",
"desc" => "If the image is located within the images folder you can just insert images/image-name.jpg, otherwise
you have to insert the full path to the image, for example: http://site.com/image-name.jpg"
),

array(
"name" => "Logo image width",
"id" => $shortname."_logo_width",
"type" => "text",
"desc" => "The logo image width in pixels- default:300px"
),

array(
"name" => "Logo image height",
"id" => $shortname."_logo_height",
"type" => "text",
"desc" => "The logo image height in pixels- default:91px"
),

array(
"name" => "Headings color",
"id" => $shortname."_heading_color",
"type" => "text",
"color" => true
),


array(
"name" => "Links color",
"id" => $shortname."_link_color",
"type" => "text",
"color" => true
),

array(
"name" => "Read More link color",
"id" => $shortname."_more_link_color",
"type" => "text",
"color" => true
),

array(
"name" => "Menu links color",
"id" => $shortname."_menu_link_color",
"type" => "text",
"color" => true
),

array(
"name" => "Menu links hover color",
"id" => $shortname."_menu_link_hover",
"type" => "text",
"color" => true
),

array(
"name" => "Menu links font size",
"id" => $shortname."_menu_link_size",
"type" => "text",
"desc" => "The menu links font size in pixels. Default: 16"
),

array(
"name" => "Sidebar menu links color",
"id" => $shortname."_sidebar_link_color",
"type" => "text",
"color" => true
),

array(
"name" => "Slidebar menu links hover color",
"id" => $shortname."_sidebar_link_hover",
"type" => "text",
"color" => true
),

array(
"name" => "Footer background color",
"id" => $shortname."_footer_bg",
"type" => "text",
"color" => true
),

array(
"name" => "Footer text color",
"id" => $shortname."_footer_text_color",
"type" => "text",
"color" => true
),

array(
"name" => "Additional CSS styles",
"id" => $shortname."_additional_styles",
"type" => "textarea",
"desc" => "You can insert some more additional CSS code here"
),

array(
"type" => "close"),

array(
"type" => "submit"),



//------------------IMAGE FADER SETTINGS-------------------

array(
"name" => "Image fader settings",
"type" => "title"),


array(
"type" => "open"),

array(
"name" => "AddFaderImage",
"type" => "add_fader_image",
),

array(
"id" => $shortname."_fader_image_names",
"type" => "hidden",
),

array(
"id" => $shortname."_fader_image_links",
"type" => "hidden",
"function" => "setFaderFunc();"
),

array(
"type" => "close"),

array(
"type" => "submit"),

//------------------ACCORDION IMAGE SLIDER SETTINGS-------------------

array(
"name" => "Accordion image slider settings",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "AddSliderImage",
"type" => "add_image",
),

array(

"id" => $shortname."_url_img",
"type" => "hidden",
),

array(

"id" => $shortname."_title_img",
"type" => "hidden",
),

array(

"id" => $shortname."_link_img",
"type" => "hidden",
),

array(

"id" => $shortname."_desc_img",
"type" => "hidden",
"function" => "setSliderFunc();"
),

array(
"type" => "close"),

array(
"type" => "submit"),


//------------------BLOG SETTINGS-------------------

array(
"name" => "Blog page settings",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Exclude categories from blog",
"id" => $shortname."_exclude_cat_from_blog",
"type" => "multicheck",
"options" => $cat_names,
"values" => $cat_ids,
"desc" => "You can select which categories not to be shown on the blog"),

array(
"name" => "Number of posts per page",
"id" => $shortname."_post_per_page_on_blog",
"type" => "text",
"std" => "5"
),

array(
"type" => "close"),

array(
"type" => "submit"),


//------------------PORTFOLIO PAGE SETTINGS-------------------

array(
"name" => "Portfolio page settings",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Turn on/off automatic thumbnail generation",
"id" => $shortname."_auto_portfolio_thumbnail",
"type" => "select",
"options" => array("on", "off"),
"values" => array("on", "off"),
"desc" => "If you turn off this functionality you will be able to add your own thumbnail images
to your portfolio items. To do this you have to create a custom field called 'thumbnail' in your
post with the full path to your image."
),

array(
"name" => "Turn on/off title link",
"id" => $shortname."_title_link",
"type" => "select",
"options" => array("on", "off"),
"values" => array("on", "off"),
"desc" => "If this functionality is turned on, the titles of your portfolio items will be links and
will link to the section which contains the content of the portfolio item post."
),

array(
"type" => "close"),

array(
"type" => "submit"),


//------------------CONTACT PAGE SETTINGS-------------------

array(
"name" => "Contact page settings",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Email to which to send contact form message",
"id" => $shortname."_email",
"type" => "text"),


array(
"name" => "Ask your question",
"id" => $shortname."_ask_question_text",
"type" => "text",
"std" => "Ask your question"
),

array(
"name" => "Name text",
"id" => $shortname."_name_text",
"type" => "text",
"std" => "Name"
),

array(
"name" => "Your e-mail text",
"id" => $shortname."_your_email_text",
"type" => "text",
"std" => "Your e-mail"
),

array(
"name" => "Question text",
"id" => $shortname."_question_text",
"type" => "text",
"std" => "Question"
),

array(
"name" => "Send text",
"id" => $shortname."_send_text",
"type" => "text",
"std" => "Send"
),

array(
"name" => "Invalid name text",
"id" => $shortname."_name_error_text",
"type" => "text",
"std" => "Please insert your name"
),

array(
"name" => "Invalid email text",
"id" => $shortname."_email_error_text",
"type" => "text",
"std" => "Please insert a valid email"
),

array(
"name" => "Invalid question text",
"id" => $shortname."_question_error_text",
"type" => "text",
"std" => "Please insert your question"
),

array(
"name" => "Message sent text",
"id" => $shortname."_message_sent_text",
"type" => "text",
"std" => "Your message has been sent"
),

array(
"type" => "close"),

array(
"type" => "submit"),

//------------------OTHER SETTINGS-------------------

array(
"name" => "Other settings",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Show comments on non-blog posts",
"id" => $shortname."_post_comments",
"type" => "checkbox"),

array(
"type" => "close"),

array(
"type" => "submit"),

//------------------TRANSLATION-------------------

array(
"name" => "Translation",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Read more text",
"id" => $shortname."_read_more",
"type" => "text",
"std" => "Read More..."
),

array(
"name" => "Learn more text",
"id" => $shortname."_learn_more_text",
"type" => "text",
"std" => "Learn More"
),

array(
"name" => "By text",
"id" => $shortname."_by_text",
"type" => "text",
"std" => "By"
),

array(
"name" => "Previous page text",
"id" => $shortname."_previous_text",
"type" => "text",
"std" => "Previous"
),

array(
"name" => "Next page text",
"id" => $shortname."_next_text",
"type" => "text",
"std" => "Next"
),

array(
"name" => "Search box text",
"id" => $shortname."_search_text",
"type" => "text",
"std" => "Search..."
),

array(
"name" => "No comments text",
"id" => $shortname."_no_comments_text",
"type" => "text",
"std" => "No comments"
),

array(
"name" => "One omment text",
"id" => $shortname."_one_comment_text",
"type" => "text",
"std" => "One comment"
),


array(
"name" => "Comments text",
"id" => $shortname."_comments_text",
"type" => "text",
"std" => "comments"
),

array(
"name" => "Leave a comment text",
"id" => $shortname."_leave_comment_text",
"type" => "text",
"std" => "Leave a comment"
),

array(
"name" => "Name text",
"id" => $shortname."_comment_name_text",
"type" => "text",
"std" => "Name"
),

array(
"name" => "Email text",
"id" => $shortname."_email_text",
"type" => "text",
"std" => "Email(will not be published)"
),

array(
"name" => "Website text",
"id" => $shortname."_website_text",
"type" => "text",
"std" => "Website"
),

array(
"name" => "Your comment text",
"id" => $shortname."_your_comment_text",
"type" => "text",
"std" => "Your comment"
),

array(
"name" => "Submit comment text",
"id" => $shortname."_submit_comment_text",
"type" => "text",
"std" => "Submit Comment"
),

array(
"name" => "Reply text",
"id" => $shortname."_reply_text",
"type" => "text",
"std" => "Reply"
),

array(
"name" => "Categories text",
"id" => $shortname."_categories_text",
"type" => "text",
"std" => "Categories:"
),

array(
"name" => "ALL text",
"id" => $shortname."_all_text",
"type" => "text",
"std" => "ALL"
),

array(
"name" => "Search results text",
"id" => $shortname."_search_results_text",
"type" => "text",
"std" => "Search results for"
),

array(
"name" => "No results found text",
"id" => $shortname."_no_results_text",
"type" => "text",
"std" => "No results found"
),

array(
"type" => "close")
);

/*Add a Theme Options Page*/
function mytheme_add_admin() {


	global $themename, $shortname, $options;

	foreach ($options as $value) {
		if(get_option($value['id'])=='' && isset($value['std'])){
			update_option( $value['id'], $value['std']);
		}
	}

	if ( $_GET['page'] == basename(__FILE__) ) {

		if ( 'save' == $_REQUEST['action'] ) {

			foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

				foreach ($options as $value) {
					if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

					header("Location: themes.php?page=functions.php&saved=true");
					die;

		} else if( 'reset' == $_REQUEST['action'] ) {

			foreach ($options as $value) {
				delete_option( $value['id'] ); }

				header("Location: themes.php?page=functions.php&reset=true");
				die;

		}
	}

	add_theme_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

	global $themename, $shortname, $options;

	if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';

	?>

<div class="wrap" style="margin: 0 auto; padding: 20px 0px 0px;">
<form method="post"><?php foreach ($options as $value) {
	switch ( $value['type'] ) {

		case "open":
			?>
<div
	style="width: 808px; background: #eee; border: 1px solid #ddd; padding: 20px; overflow: hidden; display: block; margin: 0px 0px 30px;">
			<?php break;

case "close":
	?></div>
	<?php break;

case "misc":
	?>
<div
	style="width: 808px; background: #fffde2; border: 1px solid #ddd; padding: 20px; overflow: hidden; display: block; margin: 0px 0px 30px;">
	<?php echo $value['name']; ?></div>
	<?php break;

case "title":
	?>
<div class="adminTitle"><?php echo $value['name']; ?></div>
	<?php break;

case 'text':
	?>
<div
	style="width: 808px; padding: 0px 0px 10px; margin: 0px 0px 10px; border-bottom: 1px solid #ddd; overflow: hidden;">
<span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['name']; ?> </span> <?php if ($value['image'] != "") {?>
<div style="width: 808px; padding: 10px 0px; overflow: hidden;"><img
	style="padding: 5px; background: #FFF; border: 1px solid #ddd;"
	src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>"
	alt="image" /></div>
	<?php } ?> <input style="width: 600px;"
	name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"
	type="<?php echo $value['type']; ?>"
	<?php if($value['color']==true) echo (' class="color" ');?>
	value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'] )); } else { echo stripslashes($value['std']); } ?>" />
<br />
<span
	style="font-family: Arial, sans-serif; font-size: 11px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['desc']; ?> </span></div>
	<?php
	break;

case 'hidden':
	?> <input style="width: 600px;" name="<?php echo $value['id']; ?>"
	id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>"
	value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'] )); } else { echo stripslashes($value['std']); } ?>" />
<script><?php echo($value['function']);?></script> <?php
break;

case 'textarea':
	?>
<div
	style="width: 808px; padding: 0px 0px 10px; margin: 0px 0px 10px; border-bottom: 1px solid #ddd; overflow: hidden;">
<span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['name']; ?> </span> <?php if ($value['image'] != "") {?>
<div style="width: 808px; padding: 10px 0px; overflow: hidden;"><img
	style="padding: 5px; background: #FFF; border: 1px solid #ddd;"
	src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>"
	alt="image" /></div>
	<?php } ?> <textarea name="<?php echo $value['id']; ?>"
	style="width: 400px; height: 200px;"
	type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'] )); } else { echo stripslashes($value['std']); } ?>
</textarea> <br />
<span
	style="font-family: Arial, sans-serif; font-size: 11px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['desc']; ?> </span></div>
	<?php
	break;
	/*Ralph Damiano*/
case 'select':
	?>
<div
	style="width: 808px; padding: 0px 0px 10px; margin: 0px 0px 10px; border-bottom: 1px solid #ddd; overflow: hidden;">
<span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['name']; ?> </span> <?php if ($value['image'] != "") {?>
<div style="width: 808px; padding: 10px 0px; overflow: hidden;"><img
	style="padding: 5px; background: #FFF; border: 1px solid #ddd;"
	src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>"
	alt="image" /></div>
	<?php } ?> <select style="width: 240px;"
	name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
	<?php
	$counter=0;
	foreach ($value['options'] as $option) { ?>
	<option
	<?php if ( get_settings( $value['id'] ) ==$value['values'][$counter]) {
		echo ' selected="selected"';
	} elseif ($option == $value['std']) {
		echo ' selected="selected"';
	} ?>
		value="<?php echo($value['values'][$counter]);?>"><?php echo $option; ?></option>
		<?php
		$counter++;
	} ?>
</select> <br />
<span
	style="font-family: Arial, sans-serif; font-size: 11px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['desc']; ?> </span></div>
	<?php
	break;

case "checkbox":
	?>
<div
	style="width: 808px; padding: 0px 0px 10px; margin: 0px 0px 10px; border-bottom: 1px solid #ddd; overflow: hidden;">
<span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['name']; ?> </span> <?php if ($value['image'] != "") {?>
<div style="width: 808px; padding: 10px 0px; overflow: hidden;"><img
	style="padding: 5px; background: #FFF; border: 1px solid #ddd;"
	src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>"
	alt="image" /></div>
	<?php } ?> <?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
<input type="checkbox" name="<?php echo $value['id']; ?>"
	id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
<br />
<span
	style="font-family: Arial, sans-serif; font-size: 11px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['desc']; ?> </span></div>
	<?php
	break;

case 'multicheck':
	?>
<div
	style="width: 808px; padding: 0px 0px 10px; margin: 0px 0px 10px; border-bottom: 1px solid #ddd; overflow: hidden;">
<span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['name']; ?> </span> <?php if ($value['image'] != "") {?>
<div style="width: 808px; padding: 10px 0px; overflow: hidden;"><img
	style="padding: 5px; background: #FFF; border: 1px solid #ddd;"
	src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>"
	alt="image" /></div>
	<?php } ?> <?php

	$idsString=get_settings($value['id']);

	//$idsArray=$idsString.split(",");
	$idsArray=explode(",",$idsString);
	$counter=0;
	foreach ($value['options'] as $option) { ?> <input type="checkbox"
	value="<?php echo($value['values'][$counter]); ?>" class="check"
	<?php if(in_array($value['values'][$counter], $idsArray)){echo('checked="yes" ');}?> /><?php echo $option; ?><br />

	<?php
	$counter++;
	} ?> <input type="hidden" class="hiddenText"
	name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"
	value="<?php echo(get_settings($value['id']));?>"> <br />
<span
	style="font-family: Arial, sans-serif; font-size: 11px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">
	<?php echo $value['desc']; ?> </span></div>
	<?php
	break;

case "submit":
	?>
<p class="submit"><input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" /></p>
	<?php break;

case "add_image":?> <span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">Add
Slider Image</span>
<table>
	<tr>
		<td>Image URL:</td>
		<td><input type="text" id="urlImg" /></td>
	</tr>
	<tr>
		<td>Title:</td>
		<td><input type="text" id="titleImg" /></td>
	</tr>
	<tr>
		<td>Title Link:</td>
		<td><input type="text" id="linkImg" /></td>
	</tr>
	<tr>
		<td>Short description:</td>
		<td><textarea style="width: 250px; height: 100px;" id="descImg"></textarea></td>
	</tr>
	<tr>
		<td><input type="button" id="addImageButton" value="Add Image" /></td>
	</tr>
</table>



<br />
<br />
Ddrag and drop boxes below to sort them <br />
<br />
<ul id="sortable" class="sortable">

</ul>

<?php break;

case "add_fader_image": ?> <span
	style="font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; color: #444; display: block; padding: 5px 0px;">Add
Fader Image</span>

<table>
	<tr>
		<td>Image URL:</td>
		<td><input type="text" id="urlFadeImg" /></td>
	</tr>
	<tr>
		<td>Image Link(Optional):</td>
		<td><input type="text" id="linkFadeImg" /></td>
	</tr>
	<tr>
		<td><input type="button" id="addFaderImageButton" value="Add Image" /></td>
	</tr>
</table>
<br />
<br />
Ddrag and drop boxes below to sort them <br />
<br />
<ul id="sortableFade" class="sortable">

</ul>
<?php

	}
}
?>
<p class="submit"><input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" /></p>
</form>
<form method="post">
<p class="submit"><input name="reset" type="submit" value="Reset" /> <input
	type="hidden" name="action" value="reset" /></p>
</form>
<?php
}

add_action('admin_menu', 'mytheme_add_admin');
/*End of Add a Theme Options Page*/

/*End of Theme Options =======================================*/




/**
 * Gets an option from the option settings.
 */
function getOption($option) {
	global $mytheme;
	return $mytheme->option[$option];
}





function is_admin_comment($currentAuthor){

	//Automatically pull admin accounts

	$user_level = 8; //Default user level (1-10)
	$admin_emails = array(); //Hold Admin Emails

	//Search for the ID numbers of all accounts at specified user level and up
	$admin_accounts = $wpdb->get_results("SELECT * FROM $wpdb->usermeta WHERE meta_key = 'wp_user_level' AND meta_value >= $user_level ");

	//Get the email address for each administrator via ID number
	foreach ($admin_accounts as $admin_account){

		//Get database row for current user id
		$admin_info = $wpdb->get_row("SELECT * FROM $wpdb->users WHERE ID = $admin_account->user_id");

		//Add current user's email to array
		$admin_emails[$admin_account->user_id] = $admin_info->user_email;
	}

	$admin_comment = false;
	foreach ($admin_emails as $admin_email){
		//If comment was made from an admin email
		if($currentAuthor == $admin_email){
			$admin_comment = true;
			break;
		}
	};

	return $admin_comment;

}


/**
 * Displays a single comment.
 */
function mytheme_comment($comment, $args, $depth) {

	global $wpdb;
	$user_level = 8; //Default user level (1-10)
	$admin_emails = array(); //Hold Admin Emails

	//Search for the ID numbers of all accounts at specified user level and up
	$admin_accounts = $wpdb->get_results("SELECT * FROM $wpdb->usermeta WHERE meta_key = 'wp_user_level' AND meta_value >= $user_level ");

	//Get the email address for each administrator via ID number
	foreach ($admin_accounts as $admin_account){

		//Get database row for current user id
		$admin_info = $wpdb->get_row("SELECT * FROM $wpdb->users WHERE ID = $admin_account->user_id");

		//Add current user's email to array
		$admin_emails[$admin_account->user_id] = $admin_info->user_email;
	}

	$admin_comment = false;
	foreach ($admin_emails as $admin_email){
		//If comment was made from an admin email
		if($comment->comment_author_email == $admin_email){
			$admin_comment = true;
			break;
		}
	};

	$GLOBALS['comment'] = $comment;

	?>
<li>
<div class="commentContainer">
<div class="comentBox">
<div class="commentAutor"><?php echo get_avatar($comment,$size='80',$default='<path_to_url>' ); ?>
<p class="ComentAutorName"><?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()) ?></p>
<div class="commentDate"><div class="alignleft"><?php printf(__('%1$s'), get_comment_date()) ?></div>
<div class="reply">
<div class="reply_icon"></div>
	<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => 2, reply_text=>get_option('_reply_text'))));
	?></div>
</div>

</div>
	<?php if($admin_comment==true && get_option('highlight_admin_comments')!='off') echo('<div class="adminRibbon"></div>'); ?>
<div class="commentText"><?php comment_text(); ?></div>

</div>
</div>
</li>
	<?php
}


/**
 * Returns the parent of a page. If the page does not have a parent, returns 0.
 */
function get_parentId() {
	global $post;
	echo($post->post_parent->title);
	return $post->post_parent;
};





function admin_head_add()
{
	$path = get_bloginfo('template_url');
	echo '<script type="text/javascript" src="'.$path.'/script/jquery-1.4.js"></script>';
	echo '<script type="text/javascript" src="'.$path.'/script/colorpicker.js"></script>';
	echo '<script type="text/javascript" src="'.$path.'/script/options.js"></script>';
	echo '<script type="text/javascript" src="'.$path.'/script/jquery-ui.js"></script>';
	echo '<link rel="stylesheet" href="'.get_bloginfo('template_directory').'/admin_style.css" type="text/css" media="screen" charset="utf-8" />';
	echo '<link rel="stylesheet" media="screen" type="text/css" href="'.$path.'/colorpicker.css" />';
}

if(isset($_GET['page']) && $_GET['page']=='functions.php'){
	add_action('admin_head', 'admin_head_add');
}

/**
 * Registers a dynamic sidebar.
 */
if (function_exists('register_sidebar')){
	register_sidebar(array('name'=>'blogSidebar',
        'before_widget' => '',
        'after_widget' => '</div>',
        'before_title' => ' <div class="sidebarTitile"><h2>',
        'after_title' => '</h2></div><div class="sidebarMenu">',
	));

	register_sidebar(array('name'=>'pageSidebar',
        'before_widget' => '',
        'after_widget' => '</div>',
        'before_title' => ' <div class="sidebarTitile"><h2>',
        'after_title' => '</h2></div><div class="sidebarMenu">',
	));

	register_sidebar(array('name'=>'homeSidebar',
        'before_widget' => '',
        'after_widget' => '</div>',
        'before_title' => ' <div class="sidebarTitile"><h2>',
        'after_title' => '</h2></div><div class="sidebarMenu">',
	));

	register_sidebar(array('name'=>'contactSidebar',
        'before_widget' => '',
        'after_widget' => '</div>',
        'before_title' => ' <div class="sidebarTitile"><h2>',
        'after_title' => '</h2></div><div class="sidebarMenu">',
	));

}


function get_author_avatar($id){
	global $wpdb;
	$metaQuery="SELECT * FROM ".$wpdb->prefix."usermeta WHERE user_id=".$id." AND meta_key='avatar'";
	$meta=$wpdb->get_results($metaQuery);

	foreach($meta as $m){
		if($m->meta_key=='avatar'){
			$avatar=$m->meta_value;
			return $avatar;
		}
	}
}


/**
 * Prints the post thumbnail.
 * @param $type the type of the thumbnail- used if WP version is higher than 2.9.
 * @param $class the class of the thumbnail image- used of WP version is lower than 2.9.
 * @param $post the post
 */
function print_post_thumbnail($class, $post){
	
	if(get_post_meta($post->ID, "thumbnail", $single = true)){
		?>
		<img
	src="<?php echo(get_post_meta($post->ID, "thumbnail", $single = true));?>"
	class="<?php echo($class); ?>" alt="" />
	<?php 
	}elseif(function_exists('has_post_thumbnail') && has_post_thumbnail()){
		//this is WP 2.9 or higher, use the built in post thumbnail function
		the_post_thumbnail();
	}
}




/*-----------------------SHORTCODES-----------------------*/

function show_highlight_green($atts, $content = null) {
	return '<span class="hihglight1">'.$content.'</span>';
}

function show_highlight_orange($atts, $content = null) {
	return '<span class="hihglight2">'.$content.'</span>';
}

function show_info($atts, $content = null) {
	return '<div class="info_box">'.$content.'</div>';
}

function show_note($atts, $content = null) {
	return '<div class="note_box">'.$content.'</div>';
}

function show_tip($atts, $content = null) {
	return '<div class="tip_box">'.$content.'</div>';
}

function show_error($atts, $content = null) {
	return '<div class="error_box">'.$content.'</div>';
}

function show_column_left($atts, $content = null) {
	return '<div class="columns"><div class="two_column_1">'.$content.'</div>';
}

function show_column_right($atts, $content = null) {
	return '<div class="two_column_2">'.$content.'</div></div>';
}

function show_list($atts, $content = null) {
	extract(shortcode_atts(array(
		"type" => 'lblue'
	), $atts));
	return '<div class="bullet_'.$type.'">'.$content.'</div>';
}

function show_frame($atts, $content = null) {
	return '<div class="borderImg">'.$content.'</div>';
}

function show_frame_left($atts, $content = null) {
	return '<div class="borderImg imgLeft">'.$content.'</div>';
}

function show_frame_right($atts, $content = null) {
	return '<div class="borderImg imgRight">'.$content.'</div>';
}

function show_big_letter($atts, $content = null) {
	return '<span class="big_letter">'.$content.'</span>';
}


add_shortcode('hg', 'show_highlight_green');
add_shortcode('ho', 'show_highlight_orange');
add_shortcode('info', 'show_info');
add_shortcode('note', 'show_note');
add_shortcode('tip', 'show_tip');
add_shortcode('error', 'show_error');
add_shortcode('column_left', 'show_column_left');
add_shortcode('column_right', 'show_column_right');
add_shortcode('list', 'show_list');
add_shortcode('frame', 'show_frame');
add_shortcode('frame_left', 'show_frame_left');
add_shortcode('frame_right', 'show_frame_right');
add_shortcode('bl', 'show_big_letter');







/* ADD NEW META BOXES TO THE PAGES */


$new_meta_boxes =
array(
array(
"title" => "Slider",
"name" => "slider",
"type" => "select",
"options" => array('None', 'Accordion Slider','Fader'),
"values" => array('none','slider','fader'),
"std" => 'none'
),

"image" => array(
"name" => "subtitle",
"std" => "",
"type" => "text",
"title" => "Subtitle",
"description" => "This is the subtitle that will be shown on the page if no slider is enabled"),

array(
"name" => "postCategory",
"title" => "Post Category",
"type" => "select",
"none" => true,
"options" => $cat_names,
"values" => $cat_ids,
"sdt" => 'none',
"description" => "This is the category whose posts will be shown on the page. Applicable for the <strong>Home, Featured page</strong> and the <strong>Portfolio</strong> page templates"),


array(
"name" => "categories",
"title" => "Show portfolio categories",
"type" => "select",
"options" => array('Show','Hide'),
"values" => array('on', 'off'),
"sdt" => 'show',
"description" => "<strong>Portfolio</strong> page templates only"),

array(
"title" => "Number of posts to show",
"name" => "postNumber",
"std" => "6",
"type" => "text",
"description" => "<strong>Portfolio</strong> page templates only"
),



);





	$new_meta_post_boxes =
	array(

	array(
	"title" => "Thumbnail URL",
	"name" => "thumbnail",
	"std" => "",
	"type" => "text",
	"description" => "You can place here the thumbnail image URL- for portfolio posts or if you 
	are using WordPress version lower than 2.9- for home and blog posts"
	),

	array(
"title" => "Preview Image/Video URL",
"name" => "preview_image",
"std" => "",
"type" => "text",
"description" => "Only for Portfolio Page and Portfolio Gallery posts- this is the big image that is shown after clicking
on the smaller thumbnail image."
	),
	
		array(
"title" => "Description",
"name" => "description",
"std" => "",
"type" => "text",
"description" => "Only for Portfolio posts- this is the description of the item."
	),


	array(
"title" => "Preview file type",
"name" => "video",
"std" => "",
"type" => "select",
"options" => array('Image','Video'),
"values" => array('off', 'on'),
"description" => "Only for Portfolio Page and Portfolio Gallery posts- if set to video, when hovering the item, the icon will be changed to show that this is a video."
	));



function new_meta_boxes() {
	global $post, $new_meta_boxes;

	foreach($new_meta_boxes as $meta_box) {
		print_meta_box($meta_box, $post);
	}
}

function new_meta_post_boxes() {
	global $post, $new_meta_post_boxes;

	foreach($new_meta_post_boxes as $meta_box) {
		print_meta_box($meta_box, $post);
	}
}


function print_meta_box($meta_box, $post){
	$meta_box_value = get_post_meta($post->ID, $meta_box['name'], true);

	if($meta_box_value == "")
	$meta_box_value = $meta_box['std'];

	switch($meta_box['type']){
		case 'heading':
			echo'<h2 style="font-weight:bold; margin-bottom:0px; font-style:normal; font-size:18px;">'.$meta_box['title'].'</h2><hr/>';
			break;
		case 'text':
			echo'<input type="hidden" name="'.$meta_box['name'].'_noncename" id="'.$meta_box['name'].'_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';

			echo'<h2>'.$meta_box['title'].'</h2>';

			echo'<input type="text" name="'.$meta_box['name'].'" value="'.$meta_box_value.'" size="55" /><br />';

			echo'<p><label for="'.$meta_box['name'].'">'.$meta_box['description'].'</label></p>';
			break;
		case 'select':
			echo'<input type="hidden" name="'.$meta_box['name'].'_noncename" id="'.$meta_box['name'].'_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';

			echo'<h2>'.$meta_box['title'].'</h2>';

			echo '<select name="'.$meta_box['name'].'">';

			if($meta_box['none']){
				?>
<option value="none">None</option>
				<?php
			}
			$counter=0;
			foreach ($meta_box['options'] as $option) { ?>
<option
<?php if ( $meta_box_value == $meta_box['values'][$counter]) {
	echo ' selected="selected"';
}
?>
	value="<?php echo($meta_box['values'][$counter]);?>"><?php echo $option; ?></option>
<?php
$counter++;
			}
			echo '</select>';

			echo'<p><label for="'.$meta_box['name'].'">'.$meta_box['description'].'</label></p>';

	}
}



function create_meta_box() {
	global $theme_name;
	if ( function_exists('add_meta_box') ) {
		add_meta_box( 'new-meta-boxes', '<div style="position:relative; top:-3px; margin-right:5px; width:20px; height:20px; float:left; background-image:url('.get_bloginfo('template_directory').'/images/logo_small.png); "></div>The PaperCore page settings', 'new_meta_boxes', 'page', 'normal', 'high' );
	}
}

function create_meta_post_box() {
	global $theme_name;
	if ( function_exists('add_meta_box') ) {
		add_meta_box( 'new-meta-post-boxes', '<div style="position:relative; top:-3px; margin-right:5px; width:20px; height:20px; float:left; background-image:url('.get_bloginfo('template_directory').'/images/logo_small.png); "></div>The PaperCore post settings', 'new_meta_post_boxes', 'post', 'normal', 'high' );
	}
}

function save_postdata( $post_id ) {
	global $post, $new_meta_boxes;

	foreach($new_meta_boxes as $meta_box) {
		// Verify
		if ( !wp_verify_nonce( $_POST[$meta_box['name'].'_noncename'], plugin_basename(__FILE__) )) {
			return $post_id;
		}

		if ( 'page' == $_POST['post_type'] ) {
			if ( !current_user_can( 'edit_page', $post_id ))
			return $post_id;
		} else {
			if ( !current_user_can( 'edit_post', $post_id ))
			return $post_id;
		}

		$data = $_POST[$meta_box['name']];

		if(get_post_meta($post_id, $meta_box['name']) == "")
		add_post_meta($post_id, $meta_box['name'], $data, true);
		elseif($data != get_post_meta($post_id, $meta_box['name'], true))
		update_post_meta($post_id, $meta_box['name'], $data);
		elseif($data == "")
		delete_post_meta($post_id, $meta_box['name'], get_post_meta($post_id, $meta_box['name'], true));
	}
}

function save_post_postdata( $post_id ) {
	global $post, $new_meta_post_boxes;

	foreach($new_meta_post_boxes as $meta_box) {
		// Verify
		if ( !wp_verify_nonce( $_POST[$meta_box['name'].'_noncename'], plugin_basename(__FILE__) )) {
			return $post_id;
		}

		if ( 'page' == $_POST['post_type'] ) {
			if ( !current_user_can( 'edit_page', $post_id ))
			return $post_id;
		} else {
			if ( !current_user_can( 'edit_post', $post_id ))
			return $post_id;
		}

		$data = $_POST[$meta_box['name']];

		if(get_post_meta($post_id, $meta_box['name']) == "")
		add_post_meta($post_id, $meta_box['name'], $data, true);
		elseif($data != get_post_meta($post_id, $meta_box['name'], true))
		update_post_meta($post_id, $meta_box['name'], $data);
		elseif($data == "")
		delete_post_meta($post_id, $meta_box['name'], get_post_meta($post_id, $meta_box['name'], true));
	}
}




	
?>