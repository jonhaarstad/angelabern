<div id="header">
  <div class="fadeWrapper">
    <div class="wrapper">
      <ul class="wrapper">
        <?php

$separator='|*|';

$sliderImagesString = get_option('_url_img');
$linkString=get_option('_link_img');
$titleString=get_option('_title_img');
$descString=get_option('_desc_img');
 
$sliderImagesArray=explode($separator, $sliderImagesString);
$linkArray= explode($separator,$linkString);
$titleArray= explode($separator,$titleString);
$descArray= explode($separator,$descString);
 
$count=count($sliderImagesArray);
$linkCount=count($linkArray);
 
for($i=0;$i<$count-1;$i++){
	echo(' <div class="imageHolder"> ');
	echo('<img src="');
	$path=$sliderImagesArray[$i];
	echo($path);
	echo('" alt=""/>');?>
        <div class="imgInfo">
          <h3>
            <?php if($linkArray[$i]!=''){?>
            <a
		href="<?php echo($linkArray[$i]);?>">
            <?php }?>
            <?php echo(stripslashes($titleArray[$i]));?>
            <?php if($linkArray[$i]!=''){?>
            </a>
            <?php }?>
          </h3>
          <p><?php echo(stripslashes($descArray[$i])); ?></p>
        </div>
        <div class="black"></div>
        <?php
	echo('</div>');
}
?>
      </ul>
    </div>
  </div>
</div>
