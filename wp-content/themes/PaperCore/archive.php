<?php
get_header();

?>

<div id="contentContainer">
<div id="pageHeader"></div>
<div id="content">
  <div class="postBoxs">
    <?php
if(have_posts()){
	$pageId=get_the_ID();
	while(have_posts()){
		the_post();

		$cat=get_the_category();
		$catParent=$cat[0]->category_parent;


		$blog=false;
		foreach ($cat as $c) {
			if($c->cat_ID==getOption('blog_cat_id') || $c->category_parent==getOption('blog_cat_id')){
				$blog=true;
				$catId='blog';
			}
		}
		?>
    <div class="postBox">
      <div class="info">
        <div class="date"><span class="month">
          <?php the_time('M') ?>
          </span>
          <h1>
            <?php the_time('d') ?>
          </h1>
          <h3>
            <?php the_time('Y') ?>
          </h3>
        </div>
        <h1><a href="<?php the_permalink();?>">
          <?php the_title();?>
          </a></h1>
        <div class="postInfo">
          <?php the_category(',');?>
          | <?php echo get_option('_by_text'); ?>
          <?php the_author(); ?>
          | <a href="<?php the_permalink();?>#comments">
          <?php comments_number(get_option('_no_comments_text'), get_option('_one_comment_text'), '% '.get_option('_comments_text'))?>
          </a></div>
      </div>
      <div class="postContent">
        <?php print_post_thumbnail('postImg', $post); ?>
        <?php the_content('');?>
        <a
	href="<?php the_permalink(); ?>" class="moreLink"><?php echo(get_option("_read_more")); ?></a> </div>
    </div>
     <div class="hr6">
      <hr />
    </div>
    <?php
	}      ?>
    <div id="blogNavButtons" class="navigation">
      <div class="alignleft">
        <?php previous_posts_link('<span>&laquo;</span> '.get_option('_previous_text')) ?>
      </div>
      <div class="alignright">
        <?php next_posts_link(get_option('_next_text').' <span>&raquo;</span>') ?>
      </div>
    </div>
    <?php   }else if ( is_category() ) { // If this is a category archive
		printf("<p class='center'>Sorry, but there aren't any posts in the %s category yet.</p>", single_cat_title('',false));
	} else if ( is_date() ) { // If this is a date archive
		echo("<p>Sorry, but there aren't any posts with this date.</p>");
	} else if ( is_author() ) { // If this is a category archive
		$userdata = get_userdatabylogin(get_query_var('author_name'));
		printf("<p class='center'>Sorry, but there aren't any posts by %s yet.</p>", $userdata->display_name);
	} else {
		echo("<p class='center'>No posts found.</p>");
	}
	?>
  </div>
</div>
<?php get_sidebar();
	get_footer();?>
